import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:wc_adv_fe/components/box_wrapper.dart';
import 'package:wc_adv_fe/components/header.dart';
import 'package:wc_adv_fe/components/boilerplate.dart';
import 'package:wc_adv_fe/modules/login/pages/login.dart';
import 'package:wc_adv_fe/modules/login/pages/register.dart';

class SplashPage extends HookWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _isLoginWidgetVisible = useState(false);
    final _isRegistrationWidgetVisible = useState(false);
    // function for displaying & hiding login widget
    void showLoginWidget() {
      _isLoginWidgetVisible.value =
          !_isLoginWidgetVisible.value;
      _isRegistrationWidgetVisible.value = false;
    }

    void showRegistrationWidget() {
      _isRegistrationWidgetVisible.value =
          !_isRegistrationWidgetVisible.value;
      _isLoginWidgetVisible.value = false;
    }

    return Boilerplate(
      child: _isLoginWidgetVisible.value == false &&
              _isRegistrationWidgetVisible.value == false
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/splash.png',
                    height: 300,
                    width: 300,
                  ),
                  BoxWrapper(
                    child: Column(
                      children: [
                        const HeaderText('Welcome to'),
                        const HeaderText(
                            'White Cloak Adventures'),
                        Padding(
                          padding:
                              const EdgeInsets.all(8.0),
                          child: SignInButton(
                            Buttons.Google,
                            onPressed: () {},
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.all(8.0),
                          child: SignInButton(
                            Buttons.Email,
                            onPressed: showLoginWidget,
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.all(8.0),
                          child: TextButton(
                            style: TextButton.styleFrom(
                                primary: Colors.white),
                            child: const Text('Register'),
                            onPressed:
                                showRegistrationWidget,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          : _isLoginWidgetVisible.value == true &&
                  _isRegistrationWidgetVisible.value ==
                      false
              ? LoginScreen(
                  showLoginWidget: showLoginWidget)
              : RegisterScreen(
                  showRegistrationWidget:
                      showRegistrationWidget),
    );
  }
}
