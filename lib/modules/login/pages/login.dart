import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:wc_adv_fe/components/body.dart';
import 'package:wc_adv_fe/components/box_wrapper.dart';
import 'package:beamer/beamer.dart';

class LoginScreen extends HookWidget {
  final dynamic showLoginWidget;

  const LoginScreen({
    Key? key,
    required this.showLoginWidget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _passwordVisible = useState(false);

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/splash.png',
            height: 300,
            width: 300,
          ),
          BoxWrapper(
              width: 400,
              height: 200,
              child: Column(
                children: [
                  Form(
                      child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: const InputDecoration(
                              fillColor: Colors.white,
                              filled: true,
                              icon: Icon(
                                Icons.person,
                                color: Colors.white,
                              ),
                              labelText: 'Username'),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                            obscureText:
                                !_passwordVisible.value,
                            decoration: InputDecoration(
                              fillColor: Colors.white,
                              filled: true,
                              icon: const Icon(
                                Icons.password,
                                color: Colors.white,
                              ),
                              labelText: 'Password',
                              suffixIcon: IconButton(
                                icon: Icon(
                                  _passwordVisible.value
                                      ? Icons.visibility
                                      : Icons
                                          .visibility_off,
                                  color: Colors.black,
                                ),
                                onPressed: () {
                                  _passwordVisible.value =
                                      !_passwordVisible
                                          .value;
                                },
                              ),
                            )),
                      ),
                      Row(
                        mainAxisAlignment:
                            MainAxisAlignment.spaceEvenly,
                        children: [
                          ElevatedButton(
                            onPressed: showLoginWidget,
                            child: const BodyText('Back'),
                            style: ElevatedButton.styleFrom(
                              primary: Colors.red,
                            ),
                          ),
                          ElevatedButton(
                              onPressed: () {},
                              child:
                                  const BodyText('Login'),
                              style:
                                  ElevatedButton.styleFrom(
                                primary: Colors.green,
                              ))
                        ],
                      )
                    ],
                  ))
                ],
              ))
        ],
      ),
    );
  }
}
