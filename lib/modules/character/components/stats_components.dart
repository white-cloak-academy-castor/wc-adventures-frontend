import 'package:flutter/material.dart';
import 'package:wc_adv_fe/components/body.dart';
import 'package:wc_adv_fe/components/header.dart';

class StatName extends StatelessWidget {
  final String statName;

  const StatName(this.statName, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: BodyText(
        statName,
        color: Colors.black,
      ),
    );
  }
}

class StatNumber extends StatelessWidget {
  final double statNumber;

  const StatNumber(this.statNumber, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: HeaderText(
        statNumber.toString(),
        color: Colors.black,
      ),
    );
  }
}

class StatTable extends StatelessWidget {
  final double maxHP;
  final double maxMP;
  final double strength;
  final double resistance;
  final double intelligence;
  final double dexterity;
  final double willpower;
  const StatTable(
      {Key? key,
      required this.maxHP,
      required this.maxMP,
      required this.strength,
      required this.resistance,
      required this.intelligence,
      required this.dexterity,
      required this.willpower})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Table(children: [
      TableRow(children: [
        StatName('Max HP'),
        StatNumber(maxHP),
      ]),
      TableRow(children: [
        StatName('Max MP'),
        StatNumber(maxMP),
      ]),
      TableRow(children: [
        StatName('Strength'),
        StatNumber(strength),
      ]),
      TableRow(children: [
        StatName('Resistance'),
        StatNumber(resistance),
      ]),
      TableRow(children: [
        StatName('Intelligence'),
        StatNumber(intelligence),
      ]),
      TableRow(children: [
        StatName('Dexterity'),
        StatNumber(dexterity),
      ]),
      TableRow(children: [
        StatName('Willpower'),
        StatNumber(willpower),
      ]),
    ]);
  }
}

class StatTableLevelUp extends StatelessWidget {
  final double maxHP;
  final double maxMP;
  final double strength;
  final double resistance;
  final double intelligence;
  final double dexterity;
  final double willpower;
  const StatTableLevelUp(
      {Key? key,
      required this.maxHP,
      required this.maxMP,
      required this.strength,
      required this.resistance,
      required this.intelligence,
      required this.dexterity,
      required this.willpower})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Table(children: [
      TableRow(children: [
        StatName('Max HP'),
        StatNumber(maxHP),
        ElevatedButton(
          onPressed: () {},
          child: Icon(Icons.keyboard_arrow_up, color: Colors.white),
          style: ElevatedButton.styleFrom(
            shape: CircleBorder(),
            primary: Colors.black, // <-- Splash color
          ),
        )
      ]),
      TableRow(children: [
        StatName('Max MP'),
        StatNumber(maxMP),
        ElevatedButton(
          onPressed: () {},
          child: Icon(Icons.keyboard_arrow_up, color: Colors.white),
          style: ElevatedButton.styleFrom(
            shape: CircleBorder(),
            primary: Colors.black, // <-- Splash color
          ),
        )
      ]),
      TableRow(children: [
        StatName('Strength'),
        StatNumber(strength),
        ElevatedButton(
          onPressed: () {},
          child: Icon(Icons.keyboard_arrow_up, color: Colors.white),
          style: ElevatedButton.styleFrom(
            shape: CircleBorder(),
            primary: Colors.black, // <-- Splash color
          ),
        )
      ]),
      TableRow(children: [
        StatName('Resistance'),
        StatNumber(resistance),
        ElevatedButton(
          onPressed: () {},
          child: Icon(Icons.keyboard_arrow_up, color: Colors.white),
          style: ElevatedButton.styleFrom(
            shape: CircleBorder(),
            primary: Colors.black, // <-- Splash color
          ),
        )
      ]),
      TableRow(children: [
        StatName('Intelligence'),
        StatNumber(intelligence),
        ElevatedButton(
          onPressed: () {},
          child: Icon(Icons.keyboard_arrow_up, color: Colors.white),
          style: ElevatedButton.styleFrom(
            shape: CircleBorder(),
            primary: Colors.black, // <-- Splash color
          ),
        )
      ]),
      TableRow(children: [
        StatName('Dexterity'),
        StatNumber(dexterity),
        ElevatedButton(
          onPressed: () {},
          child: Icon(Icons.keyboard_arrow_up, color: Colors.white),
          style: ElevatedButton.styleFrom(
            shape: CircleBorder(),
            primary: Colors.black, // <-- Splash color
          ),
        )
      ]),
      TableRow(children: [
        StatName('Willpower'),
        StatNumber(willpower),
        ElevatedButton(
          onPressed: () {},
          child: Icon(Icons.keyboard_arrow_up, color: Colors.white),
          style: ElevatedButton.styleFrom(
            shape: CircleBorder(),
            primary: Colors.black, // <-- Splash color
          ),
        )
      ]),
    ]);
  }
}
