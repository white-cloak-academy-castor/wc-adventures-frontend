import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:wc_adv_fe/components/box_wrapper.dart';
import 'package:wc_adv_fe/components/header.dart';

class CharacterCreation extends StatelessWidget {
  const CharacterCreation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: BoxWrapper(
      alignment: Alignment.center,
      child: SizedBox(
        width: 1000,
        child: Column(
          children: [
            HeaderText('Choose your name:'),
            SizedBox(
              child: TextField(
                style: TextStyle(fontFamily: 'Immortal', color: Colors.white),
              ),
              width: 500,
            ),
            HeaderText('Choose your class:'),
            ResponsiveGridRow(children: [
              ResponsiveGridCol(
                lg: 4,
                child: Column(
                  children: [
                    Image.asset(
                      'assets/images/warrior.png',
                      height: 300,
                    ),
                    ElevatedButton(
                        onPressed: () {},
                        child: HeaderText('Warrior'),
                        style: ElevatedButton.styleFrom(primary: Colors.black))
                  ],
                ),
              ),
              ResponsiveGridCol(
                lg: 4,
                child: Column(
                  children: [
                    Image.asset('assets/images/rogue.png', height: 300),
                    ElevatedButton(
                        onPressed: () {},
                        child: HeaderText('Rogue'),
                        style: ElevatedButton.styleFrom(primary: Colors.black))
                  ],
                ),
              ),
              ResponsiveGridCol(
                lg: 4,
                child: Column(
                  children: [
                    Image.asset('assets/images/mage.png', height: 300),
                    ElevatedButton(
                        onPressed: () {},
                        child: HeaderText('Mage'),
                        style: ElevatedButton.styleFrom(primary: Colors.black))
                  ],
                ),
              )
            ]),
          ],
        ),
      ),
    ));
  }
}
