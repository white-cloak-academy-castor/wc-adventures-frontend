import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:wc_adv_fe/components/body.dart';
import 'package:wc_adv_fe/components/box_wrapper.dart';
import 'package:wc_adv_fe/components/header.dart';

class CharacterSelect extends HookWidget {
  const CharacterSelect({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BoxWrapper(
          height: 400,
          width: 700,
          child: Container(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const HeaderText(
                  'Welcome',
                  alignment: TextAlign.center,
                ),
                const HeaderText(
                  'Account Name',
                  color: Colors.yellow,
                  alignment: TextAlign.center,
                ),
                const HeaderText('Select your character'),
                DropdownButton<String>(
                  dropdownColor: Colors.black38,
                  items: [
                    DropdownMenuItem(
                      child: BodyText(
                        'Gornar the Hero',
                        size: 15,
                      ),
                      value: 'a',
                    ),
                    DropdownMenuItem(
                      child: BodyText('Devron the Field Marshall', size: 15),
                      value: 'b',
                    )
                  ],
                  onChanged: (_) {},
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(primary: Colors.red),
                          onPressed: () {},
                          child: BodyText('Change Account')),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                          style:
                              ElevatedButton.styleFrom(primary: Colors.green),
                          onPressed: () {},
                          child: BodyText('Select Character')),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: Colors.black),
                      onPressed: () {},
                      child: BodyText('Create Character')),
                )
              ],
            ),
          )),
    );
  }
}
