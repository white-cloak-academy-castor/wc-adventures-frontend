import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:wc_adv_fe/components/body.dart';
import 'package:wc_adv_fe/components/box_wrapper.dart';
import 'package:wc_adv_fe/components/header.dart';
import 'package:wc_adv_fe/modules/character/components/stats_components.dart';

class CharacterScreen extends HookWidget {
  const CharacterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _levelingUp = useState(false);
    return Center(
      child: SingleChildScrollView(
          child: BoxWrapper(
              alignment: Alignment.center,
              child: ResponsiveGridRow(children: [
                ResponsiveGridCol(
                    lg: 4,
                    child: Image.asset(
                      'assets/images/warrior.png',
                      height: 500,
                    )),
                ResponsiveGridCol(
                    lg: 4,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Card(
                            child: Column(
                              children: [
                                HeaderText('Character Name',
                                    color: Colors.black),
                                _levelingUp.value
                                    ? StatTableLevelUp(
                                        maxHP: 20,
                                        maxMP: 20,
                                        strength: 20,
                                        resistance: 20,
                                        intelligence: 20,
                                        dexterity: 20,
                                        willpower: 20)
                                    : StatTable(
                                        maxHP: 20,
                                        maxMP: 20,
                                        strength: 20,
                                        resistance: 20,
                                        intelligence: 20,
                                        dexterity: 20,
                                        willpower: 20)
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Card(
                            child: Expanded(
                              child: GridView.count(
                                shrinkWrap: true,
                                crossAxisCount: 3,
                                children: [
                                  BodyText(
                                    'Item',
                                    color: Colors.black,
                                  ),
                                  BodyText(
                                    'Item',
                                    color: Colors.black,
                                  ),
                                  BodyText(
                                    'Item',
                                    color: Colors.black,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    )),
                ResponsiveGridCol(
                    lg: 4,
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: Colors.black),
                                onPressed: () {},
                                child: BodyText('Battle')),
                          ),
                          _levelingUp.value
                              ? Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                          primary: Colors.green),
                                      onPressed: () {
                                        _levelingUp.value = false;
                                      },
                                      child: BodyText('Confirm Level Up')),
                                )
                              : Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                          primary: Colors.black),
                                      onPressed: () {
                                        _levelingUp.value = true;
                                      },
                                      child: BodyText('Level Up')),
                                ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: Colors.black),
                                onPressed: () {},
                                child: BodyText('Log Out')),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: Colors.red),
                                onPressed: () {},
                                child: BodyText('Delete Character')),
                          )
                        ],
                      ),
                    ))
              ]))),
    );
  }
}
