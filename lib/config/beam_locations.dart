import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:wc_adv_fe/modules/login/pages/splash_page.dart';

class RouteLocationGenerator {
  static const String homeRoute = '/';
}

//OPTION A: SIMPLE LOCATION BUILDER

class SimpleLocationGenerator {
  static final simpleLocationBuilder =
      SimpleLocationBuilder(
    routes: {
      '/': (context, state) => BeamPage(
            key: const ValueKey('home'),
            title: 'White Cloak Adventure',
            child: const SplashPage(),
            type: BeamPageType.noTransition,
          ),
    },
  );
}
