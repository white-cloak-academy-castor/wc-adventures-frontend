import 'package:graphql/client.dart';
import 'package:wc_adv_fe/models/character.dart';

final _httpLink =
    HttpLink('https://whitecloak-adventures.herokuapp.com/graphql');

final graphQlClient = GraphQLClient(
    link: _httpLink,
    cache: GraphQLCache(),
    defaultPolicies: DefaultPolicies(
        query: Policies.safe(
      FetchPolicy.networkOnly,
      ErrorPolicy.none,
      CacheRereadPolicy.mergeOptimistic,
    )));

class CharactersGraphQL {
  String queryCharacters = r'''
  query GetMyCharacters {
  myCharacters{
    id
    name
    class
    level
  }
    ''';

  String mutationDeleteCharacter = r'''
  mutation DeleteCharacter {
    deleteCharacter(charId:"5a8597b1-6e9f-4bde-bcde-f7239c9ef866")
  }''';

  String queryCharacterStats = r'''
  query GetCharacterStats{
    getCharcterStats
    {
      charId
      maxhp
      maxmp
      strength
      dexterity
      resistance
      intelligence
      willpower
      
    }
  }
  ''';

  Future<List<Character>> getCharacters() async {
    final options = QueryOptions(document: gql(queryCharacters));
    final response = await graphQlClient.query(options);
    if (!response.hasException) {
      final List<Object?> data =
          response.data!['GetMyCharacters']['myCharacters'];
      final values = data.map(
          (character) => Character.fromJson(character as Map<String, dynamic>));
      return values.toList();
    } else {
      print(response.exception);
      return [];
    }
  }

  Future<List<Character>> getCharacterStats() async {
    final options = QueryOptions(document: gql(queryCharacters));
    final response = await graphQlClient.query(options);
    if (!response.hasException) {
      final List<Object?> data =
          response.data!['GetCharacterStats']['getCharacterStats'];
      final values = data.map(
          (character) => Character.fromJson(character as Map<String, dynamic>));
      return values.toList();
    } else {
      print(response.exception);
      return [];
    }
  }
}
