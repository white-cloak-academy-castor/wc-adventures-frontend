import 'package:graphql/client.dart';
import 'package:wc_adv_fe/models/user.dart';

final _httpLink =
    HttpLink('https://whitecloak-adventures.herokuapp.com/graphql');

final graphQlClient = GraphQLClient(
    link: _httpLink,
    cache: GraphQLCache(),
    defaultPolicies: DefaultPolicies(
        query: Policies.safe(
      FetchPolicy.networkOnly,
      ErrorPolicy.none,
      CacheRereadPolicy.mergeOptimistic,
    )));

class UsersGraphQL {
  String queryUser = r'''
  query currentUser {
    currentUser{
      id
      username
      email
    }
  }''';

  String queryGetMyCharacters = r'''
  query GetMyCharacters {
  myCharacters{
    id
    name
    class
    level
    stats {
      maxhp
      maxmp
      strength
      intelligence
      resistance
      dexterity
      willpower
    }
  }
  ''';

  String mutationLevelUpCharacter = r'''
  mutation LevelUpCharacter{
    levelUp(charId:"3fb7afc6-23d7-4ebc-8a67-357aaba49a80")
    {
      id
      name
      level
      currentxp
      nextlvlxp
      availableStatPts
    }
  }''';

  String mutationCreateCharacter = r'''
  mutation createCharacter {
    createCharacter (input:{
      name:"Shomat"
      class:ROGUE
    }){ name
    userId
    class
    level}
  }
  ''';

  String mutationDeleteCharacter = r'''
  mutation DeleteCharacter {
    deleteCharacter(charId:"5a8597b1-6e9f-4bde-bcde-f7239c9ef866")
  }''';

  String mutationUpdateStats = r'''
  mutation UpdateStats{
    updateCharacterStats(charId:"3fb7afc6-23d7-4ebc-8a67-357aaba49a80"
    stats:{
      strength:4
      intelligence:4
      dexterity: 4
      willpower: 4
      resistance: 4
    }) {
      charId
      maxhp
      maxmp
      strength
      intelligence
      dexterity
      willpower
      resistance
    }
  }''';

  Future<List<User>> getCharacters() async {
    final options = QueryOptions(document: gql(queryUser));
    final response = await graphQlClient.query(options);
    if (!response.hasException) {
      final List<Object?> data = response.data!['currentUser']['currentUser'];
      final values =
          data.map((user) => User.fromJson(user as Map<String, dynamic>));
      return values.toList();
    } else {
      print(response.exception);
      return [];
    }
  }

  Future<List<User>> getStats() async {
    final options = QueryOptions(document: gql(queryUser));
    final response = await graphQlClient.query(options);
    if (!response.hasException) {
      final List<Object?> data = response.data!['currentUser']['currentUser'];
      final values =
          data.map((user) => User.fromJson(user as Map<String, dynamic>));
      return values.toList();
    } else {
      print(response.exception);
      return [];
    }
  }
}
