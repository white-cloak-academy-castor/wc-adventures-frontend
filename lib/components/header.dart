import 'package:flutter/material.dart';

class HeaderText extends StatelessWidget {
  final Color? color;
  final double? size;
  final String text;
  final TextAlign? alignment;

  const HeaderText(this.text, {this.color, this.alignment, this.size, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: alignment,
      style: TextStyle(
          fontFamily: 'Immortal',
          color: color ?? Colors.white,
          fontSize: size ?? 30),
    );
  }
}
