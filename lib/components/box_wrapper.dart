import 'package:flutter/material.dart';

class BoxWrapper extends StatelessWidget {
  final Widget child;
  final EdgeInsets? padding;
  final double? width;
  final double? height;
  final Alignment? alignment;

  const BoxWrapper(
      {required this.child,
      Key? key,
      this.padding,
      this.alignment,
      this.width,
      this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: padding ?? const EdgeInsets.all(4.0),
        width: width,
        height: height,
        color: Colors.black.withOpacity(0.5),
        child: child);
  }
}
