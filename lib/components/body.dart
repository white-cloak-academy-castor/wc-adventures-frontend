import 'package:flutter/material.dart';

class BodyText extends StatelessWidget {
  final Color? color;
  final double? size;
  final String text;

  const BodyText(this.text, {this.color, this.size, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          fontFamily: 'Endor',
          color: color ?? Colors.white,
          fontSize: size ?? 20),
    );
  }
}
