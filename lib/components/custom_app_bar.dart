import 'package:flutter/material.dart';

import 'header.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  CustomAppBar({Key? key})
      : preferredSize = const Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize; // default is 56.0

  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.black,
      title: const HeaderText(
        "White Cloak Adventures",
        color: Colors.white,
      ),
      actions: const [
        MenuItem(route: '/', name: 'Placeholder'),
        MenuItem(route: '/', name: 'Placeholder')
      ],
    );
  }
}

class MenuItem extends StatelessWidget {
  final String route;
  final String name;

  const MenuItem({Key? key, required this.route, required this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Text(
        name,
        style: const TextStyle(fontFamily: 'Endor', color: Colors.white),
      ),
      onPressed: () {
        Navigator.pushNamed(context, route);
      },
    );
  }
}
