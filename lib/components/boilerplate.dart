import 'package:flutter/material.dart';

class Boilerplate extends StatelessWidget {
  final Widget child;
  const Boilerplate({required this.child, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        Container(
            alignment: Alignment.center,
            constraints: const BoxConstraints.expand(),
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/main.jpg"),
                    fit: BoxFit.cover)),
            child: child),
      ],
    ));
  }
}
