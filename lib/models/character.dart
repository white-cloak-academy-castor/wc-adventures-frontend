class Character {
  final String charId;
  final String name;
  final int level;
  final String charClass;
  final CharacterStats stats;

  Character(
      {required this.charId,
      required this.name,
      required this.level,
      required this.charClass,
      required this.stats});

  factory Character.fromJson(Map<String, dynamic> json) {
    return Character(
        charId: json['id'] as String,
        name: json['name'] as String,
        level: json['level'] as int,
        charClass: json['class'] as String,
        stats: CharacterStats.fromJson(json['characterStats']));
  }
}

class CharacterStats {
  final String charId;
  final int maxHP;
  final int maxMP;
  final int strength;
  final int dexterity;
  final int resistance;
  final int intelligence;
  final int willpower;
  CharacterStats(
      {required this.charId,
      required this.maxHP,
      required this.maxMP,
      required this.strength,
      required this.dexterity,
      required this.resistance,
      required this.intelligence,
      required this.willpower});

  factory CharacterStats.fromJson(Map<String, dynamic> json) {
    return CharacterStats(
        charId: json['charId'] as String,
        maxHP: json['maxhp'] as int,
        maxMP: json['maxmp'] as int,
        strength: json['strength'] as int,
        dexterity: json['dexterity'] as int,
        resistance: json['resistance'] as int,
        intelligence: json['intelligence'] as int,
        willpower: json['willpower'] as int);
  }
}
