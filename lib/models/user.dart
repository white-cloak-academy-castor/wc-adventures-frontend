import 'package:wc_adv_fe/models/character.dart';

class User {
  String userId;
  String username;
  String email;

  User({
    required this.userId,
    required this.username,
    required this.email,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        userId: json['id'] as String,
        username: json['name'] as String,
        email: json['email'] as String);
  }
}
