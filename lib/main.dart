import 'package:flutter/material.dart';
import 'package:beamer/beamer.dart';
import 'package:url_strategy/url_strategy.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:wc_adv_fe/components/boilerplate.dart';
import 'package:wc_adv_fe/config/beam_locations.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:wc_adv_fe/modules/character/pages/character_stats.dart';

final FlutterAppAuth appAuth = FlutterAppAuth();
const FlutterSecureStorage secureStorage = FlutterSecureStorage();

Future<void> main() async {
  setPathUrlStrategy();
  // await dotenv.load();
  runApp(
    ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  final routeDelegate = BeamerDelegate(
    locationBuilder: SimpleLocationGenerator.simpleLocationBuilder,
  );
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationParser: BeamerParser(),
      routerDelegate: routeDelegate,
      backButtonDispatcher: BeamerBackButtonDispatcher(
        delegate: routeDelegate,
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
