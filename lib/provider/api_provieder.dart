import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:wc_adv_fe/api/characters.graphql.dart';

final charactersApiProvider =
    Provider<CharactersApiProvider>((ref) => CharactersApiProvider());

class CharactersApiProvider {
  final characters = CharactersGraphQL();
}
