import 'package:flutter/cupertino.dart';

import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:wc_adv_fe/api/characters.graphql.dart';
import 'package:wc_adv_fe/models/character.dart';
import 'package:wc_adv_fe/provider/api_provieder.dart';

final characterProvider = ChangeNotifierProvider((ref) {
  final charactersGraphQL = ref.watch(charactersApiProvider).characters;

  return CharacterChangeNotifier(charactersApi: charactersGraphQL);
});

class CharacterChangeNotifier extends ChangeNotifier {
  final CharactersGraphQL charactersApi;
  CharacterChangeNotifier({
    required this.charactersApi,
  });
  List<Character> _characters = [];
  List<Character> get characters => _characters;

  Future<List<Character>> getCharacters() async {
    final data = await charactersApi.getCharacters();

    _characters = [..._characters, ...data];
    return _characters;
  }
}
